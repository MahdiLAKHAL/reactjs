export const books = [
  {
    id: 1,
    author: ' Amelia Hepworth',
    title: 'I love you to the moon and back',
    image:
      'https://images-na.ssl-images-amazon.com/images/I/517h-u1AQlL._SX482_BO1,204,203,200_.jpg',
  },

  {
    id: 2,
    author: 'Dana Perino',
    title: 'Everything Will Be Okay',
    image:
      'https://images-na.ssl-images-amazon.com/images/I/51dW+WwbUDL._SX329_BO1,204,203,200_.jpg',
  },
  {
    id: 3,
    author: 'Walter Isaacson',
    title: 'The Code Breaker: Jennifer Doudna, Gene Editing',
    image:
      'https://images-na.ssl-images-amazon.com/images/I/41KMmXBPckL._SX327_BO1,204,203,200_.jpg',
  },
];
