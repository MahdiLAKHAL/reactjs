import React from 'react';

function Book(props) {
  console.log(props);
  const { image, title, author } = props.book;
  // attributen eventHandler

  function clickHandler(e) {
    console.log(e);
    console.log(e.target);
    alert('Hello World');
  }

  function overHandler(title) {
    console.log(title);
  }
  return (
    // In case using a function with params u need to code it like bellow
    <div className='card col-4' onMouseOver={() => overHandler(title)}>
      <img className='card-img-top' src={image} alt='' />
      <div className='card-body'>
        <h5 className='card-title'>{title}</h5>
        <p className='card-text'>{author}</p>
        <button className='btn btn-primary' onClick={clickHandler}>
          Reference Example
        </button>
      </div>
    </div>
  );
}
// we use DEFAULT that we can rename our module whathever we want in the destitination folder
export default Book;
