import React from 'react';
import ReactDom from 'react-dom';

// Importing css
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';

// Importing Books.js
import { books } from './books';
// Import the component book
import Book from './Book';

// JSX Rules
// return single element
// dic /section / article or fragment
// use camelCase for html attribute
// className instead of class
// close every element
// formatting
// don't place Div everywhere ;p
// React.fragment for envolopping thi html code
// Nested Components, React tools

function BookList() {
  return (
    <section className='container row'>
      {books.map((book) => {
        return <Book key={book.id} book={book} />;
      })}
    </section>
  );
}

ReactDom.render(<BookList />, document.getElementById('root'));
